import { all } from 'redux-saga/effects'

import { todos } from '../modules/todos'

/**
 * Root saga - combines sagas from multiple modules and runs it in store.
 * @link https://redux-saga.js.org/docs/advanced/RootSaga.html
 */
export const rootSaga = function*() {
    yield all([todos.saga()]) // TODO yield fork
}
