import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'

import { rootSaga } from './saga'
import {rootReducer} from './reducer'

const sagaMiddleware = createSagaMiddleware()
const middleware = [sagaMiddleware]

export const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(...middleware))
)

sagaMiddleware.run(rootSaga)

