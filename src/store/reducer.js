import { combineReducers } from 'redux'
import { reducer as form } from 'redux-form'

import { todos } from '../modules/todos'
import { alerts } from '../modules/alerts'
import { fetching } from '../modules/fetching'

/**
 * Root reducer - combines reducers from selected modules.
 */
export const rootReducer = combineReducers({
    todos: todos.reducer,
    alerts: alerts.reducer,
    api: fetching.reducer,
    form,
})
