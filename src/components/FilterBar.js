import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'

import { BulkActions, Filter } from '../components'


/**
 * Combines bulk actions and filter into a single component.
 */
export const FilterBar = () => (
    <Container className="mb-2 px-0 px-sm-3 animated fadeIn">
        <Row className="">
            <Col
                sm={6}
                className="px-sm-0 pb-3 pb-sm-0 text-center text-sm-left"
            >
                <BulkActions />
            </Col>

            <Col sm={6} className="px-sm-0 text-center text-sm-right">
                <Filter />
            </Col>
        </Row>
    </Container>
)
