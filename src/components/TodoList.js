import React from 'react'
import { connect } from 'react-redux'
import { PropTypes } from 'prop-types'
import { Spinner } from 'react-bootstrap'

import { getVisibleTodos, getTotalTodosAmount } from '../modules/todos'
import {createLoaderSelector, LOADER_FETCH_TODOS} from '../modules/fetching';
import { TodoItem, FilterBar } from '../components'

/**
 * Renders a list of todo items.
 * @param $0
 * @param $0.visibleTodos - selector, filtered array of todos ready to be displayed.
 * @param $0.todosTotalAmount - selector, total amount of todo items.
 * @param $0.isFetching - selector, boolean status of the api fetching request
 */
const TodoListComponent = ({ visibleTodos, todosTotalAmount, isFetching }) => {
    const noDataMessage = !isFetching && (
        <span className="text-center animated fadeIn">
            No tasks to display.
        </span>
    )

    const todoList = visibleTodos.length
        ? visibleTodos.map(todo => <TodoItem key={todo.id} todo={todo} />)
        : noDataMessage

    // Show spinner when api data
    const spinner = isFetching && (
        <Spinner className="mx-auto" animation="border" variant="primary" />
    )

    const filterComponent = !!todosTotalAmount && <FilterBar />

    return (
        <div>
            {filterComponent}
            <div className="text-center">
                {spinner}
                {todoList}
            </div>
        </div>
    )
}

TodoListComponent.propTypes = {
    // eslint-disable-next-line react/require-default-props
    visibleTodos: PropTypes.arrayOf(PropTypes.object),
    todosTotalAmount: PropTypes.number.isRequired,
    isFetching: PropTypes.bool.isRequired,
}

const mapStateToProps = state => ({
    visibleTodos: getVisibleTodos(state),
    todosTotalAmount: getTotalTodosAmount(state),
    isFetching: createLoaderSelector(LOADER_FETCH_TODOS)(state)
})

export const TodoList = connect(mapStateToProps)(TodoListComponent)
