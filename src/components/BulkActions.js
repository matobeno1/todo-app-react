import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Button, ButtonGroup } from 'react-bootstrap'

import { ButtonSpinner } from '../components'
import {
    requestCompleteAll,
    requestDeleteCompleted,
    getIncompleteTodosIds,
    getCompleteTodosIds,
} from '../modules/todos'
import {
    createLoaderSelector,
    LOADER_COMPLETE_ALL,
    LOADER_DELETE_COMPLETED,
} from '../modules/fetching'

/**
 * Renders two buttons (complete all, delete completed).
 * @param $0
 * @param $0.incompleteTodosIds - selector for array of incomplete todo IDs
 * @param $0.completedTodosIds - selector for array of completed todo IDs
 * @param $0.requestDeleteCompleted - bound method, dispatches deletion of completed todos
 * @param $0.requestCompleteAll - bound method, dipatches completion of incomplete todos
 * @param $0.loadingComplete - selector for loader/spinner of complete all button
 * @param $0.loadingDelete - selector for loader/spinner of delete completed button
 */
const BulkActionsComponent = ({
    incompleteTodosIds,
    completedTodosIds,
    requestDeleteCompleted,
    requestCompleteAll,
    loadingComplete,
    loadingDelete,
}) => {
    function handleDeleteCompleted() {
        requestDeleteCompleted(completedTodosIds)
    }

    function handleCompleteAll() {
        requestCompleteAll(incompleteTodosIds)
    }

    return (
        <ButtonGroup className="w-xs-100">
            <Button variant="success" onClick={handleCompleteAll}>
                {loadingComplete && <ButtonSpinner />}
                <span className="px-2">Complete all</span>
            </Button>
            <Button variant="warning" onClick={handleDeleteCompleted}>
                {loadingDelete && <ButtonSpinner />}
                <span className="px-2">Delete completed</span>
            </Button>
        </ButtonGroup>
    )
}

BulkActionsComponent.propTypes = {
    incompleteTodosIds: PropTypes.arrayOf(PropTypes.string).isRequired,
    completedTodosIds: PropTypes.arrayOf(PropTypes.string).isRequired,
}

const mapStateToProps = state => ({
    incompleteTodosIds: getIncompleteTodosIds(state),
    completedTodosIds: getCompleteTodosIds(state),
    loadingComplete: createLoaderSelector(LOADER_COMPLETE_ALL)(state),
    loadingDelete: createLoaderSelector(LOADER_DELETE_COMPLETED)(state),
})

const mapDispatchToProps = {
    requestDeleteCompleted,
    requestCompleteAll,
}

export const BulkActions = connect(
    mapStateToProps,
    mapDispatchToProps
)(BulkActionsComponent)
