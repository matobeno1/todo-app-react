import React, { Fragment } from 'react'
import { PropTypes } from 'prop-types'
import { OverlayTrigger, Button, Tooltip } from 'react-bootstrap'

import { DateString } from '../components'

/**
 * Renders a tooltip with more information about the given todo.
 * @param $0
 * @param $0.created - prop, timestamp - time when todo was created
 * @param $0.completed - prop, timestamp - time when todo was completed
 */
export const TodoExtraInfo = ({ created, completed }) => {
    const createdDateComponent = (
        <span>
            created: <DateString timestamp={created} />
        </span>
    )
    const completedDateComponent = completed && (
        <span>
            completed: <DateString timestamp={completed} />
        </span>
    )

    const tooltip = (
        <Tooltip id="tooltip" className="tooltip">
            {createdDateComponent}
            <br />
            {completedDateComponent}
        </Tooltip>
    )

    return (
        <Fragment>
            <OverlayTrigger
                placement="top"
                trigger="hover"
                delay={{ show: 150, hide: 50 }}
                overlay={tooltip}
            >
                <Button variant="link" className="px-1">
                    <i
                        className="fa icon text-dark fa-info-circle"
                        aria-hidden="true"
                    />
                </Button>
            </OverlayTrigger>
        </Fragment>
    )
}

TodoExtraInfo.propTypes = {
    created: PropTypes.number.isRequired,
    completed: PropTypes.number,
}
