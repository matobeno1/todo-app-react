import React, { useState, useRef } from 'react'
import { connect } from 'react-redux'
import { PropTypes } from 'prop-types'
import { Row, Col, Button, Card, Form } from 'react-bootstrap'

import { TodoExtraInfo } from '../components'
import {
    requestToggleTodo,
    requestDeleteTodo,
    requestEditTodo,
} from '../modules/todos'
import './TodoItem.scss'

const editFieldStyle = {
    fontSize: '1.2rem',
    height: 'auto',
}

const ENTER = 13
const ESCAPE = 27

/**
 * Renders a single todo item.
 * @param $0
 * @param $0.todo - object representing a todo, passed as a prop
 * @param $0.requestToggleTodo - bound method for requesting todo toggle
 * @param $0.requestDeleteTodo - bound method for requesting todo delete
 * @param $0.requestEditTodo - bound method for requesting todo edit
 */
const TodoItemComponent = ({
    todo,
    requestToggleTodo,
    requestDeleteTodo,
    requestEditTodo,
}) => {
    const [text, setText] = useState(todo.text)
    const [editMode, setEditMode] = useState(false)
    const editInput = useRef(null)

    function handleTodoToggle() {
        requestToggleTodo(todo.id, !todo.completed)
    }

    /**
     * Handles when user presses a key when the component has focus.
     * @param e - event
     */
    function handleKeyPress(e) {
        if (e.keyCode === ENTER) {
            // Matches empty string and returns if that's the case.
            if (/^(\s)*$/.test(text)) {
                return
            }

            // If there was any change, do the request
            if (todo.text !== text) {
                requestEditTodo(todo.id, text)
            }

            // Finish editing
            setEditMode(false)
        }

        if (e.keyCode === ESCAPE) {
            // If cancelled editing, restore previous text
            setText(todo.text)
            setEditMode(false)
        }
    }

    function handleTodoDelete() {
        requestDeleteTodo(todo.id)
    }

    function handleDoubleClick() {
        setEditMode(true)

        // Waits a little while before focusing on the element
        setTimeout(() => {
            if (!editInput.current) return
            editInput.current.focus()
        }, 20)
    }

    const textField = !editMode && (
        <span className="todo-item-text" onDoubleClick={handleDoubleClick}>
            {todo.text}
        </span>
    )

    const editField = editMode && (
        <Form.Control
            ref={editInput}
            style={editFieldStyle}
            type="text"
            onChange={e => setText(e.target.value)}
            value={text}
            onKeyDown={handleKeyPress}
        />
    )

    const { completed, completedDate, createdDate } = todo

    return (
        <Card
            className={`py-2 px-3 mb-1 animated fadeInUp faster ${
                completed ? 'complete' : 'incomplete'
            }`}
        >
            <Row>
                <Col xs={2} sm={1} className="text-center align-vertical">
                    <input
                        type="checkbox"
                        className="mx-auto custom-checkbox"
                        checked={completed}
                        onChange={handleTodoToggle}
                    />
                </Col>
                <Col
                    xs={10}
                    sm={{ span: 1, order: 2 }}
                    className="align-vertical justify-content-end"
                >
                    <TodoExtraInfo
                        className="px-1"
                        completed={completedDate}
                        created={createdDate}
                    />
                    <Button
                        variant="link"
                        className="btn-delete px-1"
                        onClick={handleTodoDelete}
                    >
                        <i
                            className="fa icon text-danger fa-trash"
                            aria-hidden="true"
                        />
                    </Button>
                </Col>
                <Col
                    xs={12}
                    sm={{ span: 10, order: 1 }}
                    className="align-vertical"
                >
                    {textField}
                    {editField}
                </Col>
            </Row>
        </Card>
    )
}

TodoItemComponent.propTypes = {
    todo: PropTypes.object.isRequired,
    requestToggleTodo: PropTypes.func.isRequired,
    requestDeleteTodo: PropTypes.func.isRequired,
    requestEditTodo: PropTypes.func.isRequired,
}

const mapDispatchToProps = {
    requestToggleTodo,
    requestDeleteTodo,
    requestEditTodo,
}

export const TodoItem = connect(
    undefined,
    mapDispatchToProps
)(TodoItemComponent)
