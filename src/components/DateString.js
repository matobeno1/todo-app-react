import React from 'react'
import PropTypes from 'prop-types'

/**
 * Converts a UNIX timestamp to a human readable date.
 * @param timestamp - integer, UNIX timestamp.
 * @returns {string} - readable date.
 */
const toDate = timestamp => new Date(timestamp).toLocaleString()


/**
 * Renders a date in human readable form.
 * @param $0
 * @param $0.timestamp - integer, a timestamp
 */
export const DateString = ({ timestamp }) => {
    const date = toDate(timestamp);
    return <span>{date}</span>
}

DateString.propTypes = {
    timestamp: PropTypes.number.isRequired,
}
