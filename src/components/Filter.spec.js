import React from 'react'
import { describe } from 'mocha'
import { shallow, configure } from 'enzyme'
import chai, { expect } from 'chai'
import chaiEnzyme from 'chai-enzyme'
import Adapter from 'enzyme-adapter-react-16'

import { FilterComponent } from './Filter'

configure({ adapter: new Adapter() })
chai.use(chaiEnzyme())

describe('Filter', () => {
    const component = shallow(
        <FilterComponent
            filterName="all"
            totalAmount={0}
            completedAmount={0}
            incompleteAmount={0}
        />
    )
    let buttonGroup;
    before(() => {
        buttonGroup = component.find('ButtonGroup')
        expect(buttonGroup.exists(), 'Button group could not be found').to.be.true
    })

    it('should contain three buttons', () => {
        expect(buttonGroup.children()).to.have.lengthOf(3)
    })

    it('every filter button should have a value property', () => {
        const children  = buttonGroup.children();

        children.forEach(child => {
            expect(child.prop('value')).to.not.be.empty;
        })
    });
})
