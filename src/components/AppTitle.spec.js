import React from 'react';
import {describe} from 'mocha';
import {shallow, configure} from 'enzyme';
import chai, {expect} from 'chai'
import chaiEnzyme from 'chai-enzyme'
import Adapter from 'enzyme-adapter-react-16';

import {AppTitle} from './AppTitle';

configure({ adapter: new Adapter() });
chai.use(chaiEnzyme())

describe('AppTitle', () => {
    const component = shallow(<AppTitle/>)
    it('should have exactly one html element', function () {
        expect(component.children()).to.have.lengthOf(1)
    });

    it('should have class text-center', () => {
        expect(component.hasClass('text-center')).to.be.true
    });

    it('should have text "MyTodo app"', () => {
        const requiredText = "MyTodo app"
        const span = component.find('span')
        expect(span.text()).to.be.equal(requiredText)
    });
})
