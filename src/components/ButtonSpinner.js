import React from 'react'
import { Spinner } from 'react-bootstrap'

export const ButtonSpinner = () => {
    return (
        <span>
            <Spinner
                style={{ marginBottom: '.15rem' }}
                as="span"
                animation="border"
                size="sm"
                role="status"
                aria-hidden="true"
            />
            <span className="sr-only">Loading...</span>
        </span>
    )
}
