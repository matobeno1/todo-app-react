import React from 'react'

export const AppTitle = () => (
    <h1 className="text-center pb-sm-3 animated fadeIn fast">
        <span>MyTodo app</span>
    </h1>
)
