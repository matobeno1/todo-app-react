import React, { useState } from 'react'
import { connect } from 'react-redux'
import { PropTypes } from 'prop-types'
import { Button, Form, Row, Col, Container } from 'react-bootstrap'

import { requestCreateTodo } from '../modules/todos'
import { ButtonSpinner } from '../components'

/**
 * Renders an input box and a button for new todo submission.
 * @param $0
 * @param $0.requestCreateTodo - bound method, dispatches action that creates new todo item
 */
const CreateTodoComponent = ({ requestCreateTodo }) => {
    const [text, setText] = useState('')
    const [loading] = useState(false)

    function handleSubmit(e) {
        e.preventDefault()
        if (!text) return
        requestCreateTodo(text)

        // Reset text after submitting
        setText('')
    }

    const buttonText = loading ? <ButtonSpinner /> : 'Create'

    return (
        <Container className="mb-3">
            <Form onSubmit={handleSubmit}>
                <Row>
                    <Col lg={10} className="px-0 pr-lg-2">
                        <Form.Group controlId="formCreateTodo">
                            <Form.Control
                                value={text}
                                autoFocus
                                onChange={e => setText(e.target.value)}
                                size="lg"
                                type="text"
                                placeholder="Type in a task to do ..."
                            />
                        </Form.Group>
                    </Col>
                    <Col lg={2} className="px-0 text-center">
                        <Button
                            variant="primary"
                            size="lg"
                            className="w-100"
                            onClick={handleSubmit}
                        >
                            {buttonText}
                        </Button>
                    </Col>
                </Row>
            </Form>
        </Container>
    )
}

CreateTodoComponent.propTypes = {
    requestCreateTodo: PropTypes.func.isRequired,
}

const mapDispatchToProps = {
    requestCreateTodo,
}

export const CreateTodo = connect(
    undefined,
    mapDispatchToProps
)(CreateTodoComponent)
