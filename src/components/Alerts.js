import React from 'react'
import { PropTypes } from 'prop-types'
import { connect } from 'react-redux'
import { Alert } from 'react-bootstrap'

import { dismissError } from '../modules/alerts'

/**
 * Component displays error messages in a dismissable panel.
 * @param $0
 * @param $0.alerts - A string containing the error message.
 * @param $0.dismissError - bound method which dispatches action to dismiss the error.
 */
const ErrorMessageComponent = ({ alerts, dismissError }) =>
    alerts && (
        <Alert
            variant="danger"
            className="animated fadeIn faster"
            onClose={() => dismissError()}
            dismissible
        >
            {alerts}
        </Alert>
    )

ErrorMessageComponent.propTypes = {
    error: PropTypes.string,
    dismissError: PropTypes.func.isRequired,
}

ErrorMessageComponent.defaultProps = {
    error: ''
}

const mapStateToProps = ({ alerts }) => ({
    alerts,
})

const mapDispatchToProps = {
    dismissError,
}

export const Alerts = connect(
    mapStateToProps,
    mapDispatchToProps
)(ErrorMessageComponent)
