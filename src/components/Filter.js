import React from 'react'
import { PropTypes } from 'prop-types'
import { connect } from 'react-redux'
import { ButtonGroup, Button } from 'react-bootstrap'

import {
    filterTodos,
    FILTER_ALL,
    FILTER_COMPLETE,
    FILTER_INCOMPLETE,
    getTotalTodosAmount,
    getCompletedTodosAmount,
    getIncompleteTodosAmount,
} from '../modules/todos/'

/**
 * Renders a group of three buttons which control the todo filter.
 * @param $0
 * @param $0.filterName - selector, name of a current active filter.
 * @param $0.totalAmount - selector, total amount of todos.
 * @param $0.completedAmount - selector, amount of completed todos.
 * @param $0.incompleteAmount - selector, amount of in-complete todos.
 * @param $0.filterTodos - bound method, changes the active filter to a different one
 */
export const FilterComponent = ({
    filterName,
    totalAmount,
    completedAmount,
    incompleteAmount,
    filterTodos,
}) => {
    function handleFilterChange(e) {
        filterTodos(e.target.value)
    }

    const buttons = [
        {
            name: FILTER_ALL,
            amount: totalAmount,
        },
        {
            name: FILTER_COMPLETE,
            amount: completedAmount,
        },
        {
            name: FILTER_INCOMPLETE,
            amount: incompleteAmount,
        },
    ]

    // The three filter buttons
    const mappedButtons = buttons.map(btn => (
        <Button
            className={`${filterName === btn.name ? 'active' : ''}`}
            key={btn.name}
            value={btn.name}
            onClick={handleFilterChange}
        >
            {btn.name} ({btn.amount})
        </Button>
    ))

    return (
        <ButtonGroup variant="primary" className="buttons button-group ml-sm-auto w-xs-100">
            {mappedButtons}
        </ButtonGroup>
    )
}

FilterComponent.propTypes = {
    filterName: PropTypes.string.isRequired,
    totalAmount: PropTypes.number.isRequired,
    completedAmount: PropTypes.number.isRequired,
    incompleteAmount: PropTypes.number.isRequired,
}

const mapStateToProps = state => ({
    filterName: state.todos.todoFilter,
    totalAmount: getTotalTodosAmount(state),
    completedAmount: getCompletedTodosAmount(state),
    incompleteAmount: getIncompleteTodosAmount(state),
})

const mapDispatchToProps = {
    filterTodos,
}

export const Filter = connect(
    mapStateToProps,
    mapDispatchToProps
)(FilterComponent)
