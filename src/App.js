import React from 'react'
import { Provider } from 'react-redux'
import { Container } from 'react-bootstrap'

import { store } from './store'
import { AppTitle, Alerts, CreateTodo, TodoList } from './components'
import './App.scss'

export const App = () => {
    return (
        <Provider store={store}>
            <Container className="app no-select">
                <AppTitle />
                <Alerts />
                <CreateTodo />
                <TodoList />
            </Container>
        </Provider>
    )
}


