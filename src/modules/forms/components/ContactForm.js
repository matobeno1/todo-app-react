import React from 'react'
import { Field, reduxForm } from 'redux-form'

import { CONTACT_FORM } from '../constants'

const ContactFormComponent = ({ handleSubmit }) => {
    return (
        <form onSubmit={handleSubmit}>
            <div>
                <label htmlFor="firstName">First Name</label>
                <Field name="firstName" component="input" type="text" />
            </div>
            <div>
                <label htmlFor="lastName">Last Name</label>
                <Field name="lastName" component="input" type="text" />
            </div>
            <button type="submit">Submit</button>
        </form>
    )
}

export const ContactForm = reduxForm({
    form: CONTACT_FORM,
})(ContactFormComponent)
