import { NAME } from './constants'

export * from './saga'

/**
 * API MODULE
 * Handles all the async calls to backend.
 * Displays errors of failed requests.
 */
export const api = {
    NAME,
}
