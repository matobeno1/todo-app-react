import { expectSaga } from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import { throwError } from 'redux-saga-test-plan/providers'

import { fetchTodos, displayErrorMessage } from './saga'
import * as requests from './requests'

describe('fetchTodos', () => {
    it('should fetch todos', () => {
        return expectSaga(fetchTodos)
            .call(requests.fetchTodos)
            .run()
    })

    it('should put error on exception throw', () => {
        const error = new Error('error')
        return expectSaga(fetchTodos)
            .provide([
                [matchers.call.fn(requests.fetchTodos), throwError(error)],
            ])
            .put(displayErrorMessage("Can't load todos", error.message))
            .run()
    })
})
