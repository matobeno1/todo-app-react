import _axios from 'axios'

const API_ROOT = process.env.REACT_APP_API_ROOT

const axios = _axios.create({
    baseURL: API_ROOT,
    responseType: 'json',
    timeout: 7000,
    headers: {
        'Content-Type': 'application/json',
    },
})

export const fetchTodos = () =>
    axios({
        method: 'GET',
        url: '/todos/',
    })

export const setComplete = (id, complete) =>
    axios({
        method: 'POST',
        url: `/todos/${id}/${complete ? 'complete' : 'incomplete'}`,
    })

export const deleteTodo = id =>
    axios({
        method: 'DELETE',
        url: `/todos/${id}`,
    })

export const editTodo = (id, text) =>
    axios({
        method: 'POST',
        url: `/todos/${id}`,
        data: {
            text,
        },
    })

export const createTodo = text =>
    axios({
        method: 'POST',
        url: '/todos/',
        data: {
            text,
        },
    })
