import { all, call, put } from 'redux-saga/effects'

import * as requests from './requests'
import { displayError } from '../alerts'

/**
 * Returns string of messages combined with a "-".
 * @param args - any amount of arguments.
 * @returns {{type, error}} - an action to display error.
 */
export const displayErrorMessage = (...args) => {
    return displayError(args.join(' - '))
}

export function* fetchTodos() {
    try {
        const {data} = yield call(requests.fetchTodos)
        return data
    } catch (e) {
        yield put(displayErrorMessage("Can't load todos", e.message))
    }
}

export function* toggleTodo(id, completed) {
    try {
        const { data } = yield call(requests.setComplete, id, completed)
        return data
    } catch (e) {
        yield put(displayErrorMessage("Can't toggle todo", e.message))
    }
}

export function* completeAll(ids) {
    try {
        yield all(
            ids.map(id => {
                return call(requests.setComplete, id, true)
            })
        )
        return true
    } catch (e) {
        yield put(displayErrorMessage("Can't complete all todos", e.message))
    }
}

export function* deleteTodo(id) {
    try {
        yield call(requests.deleteTodo, id)
        return true
    } catch (e) {
        yield put(displayErrorMessage("Can't delete todo", e.message))
    }
}

export function* deleteCompleted(ids) {
    try {
        yield all(
            ids.map(id => {
                return call(requests.deleteTodo, id)
            })
        )
        return true
    } catch (e) {
        yield put(displayErrorMessage("Can't delete complete todos", e.message))
    }
}

export function* editTodo(id, text) {
    try {
        yield call(requests.editTodo, id, text)
        return true
    } catch (e) {
        yield put(displayErrorMessage("Can't edit todo", e.message))
    }
}

export function* createTodo(text) {
    try {
        const { data } = yield call(requests.createTodo, text)
        return data
    } catch (e) {
        yield put(displayErrorMessage("Can't create todo", e.message))
    }
}
