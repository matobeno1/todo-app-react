import { DISMISS_ERROR, DISPLAY_ERROR } from './constants'

export const reducer = (state = null, action) => {
    switch (action.type) {
        case DISPLAY_ERROR:
            return action.error
        case DISMISS_ERROR:
            return null
        default:
            return state
    }
}
