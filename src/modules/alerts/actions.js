import { DISMISS_ERROR, DISPLAY_ERROR } from './constants'

/**
 * Creates action that will show error on the screen.
 * @param error - string, the error text.
 */
export const displayError = (error) => ({
    type: DISPLAY_ERROR,
    error,
})

/**
 * Creates action that will hide currently visible error message.
 * @returns {{type: *}}
 */
export const dismissError = () => ({
    type: DISMISS_ERROR,
})
