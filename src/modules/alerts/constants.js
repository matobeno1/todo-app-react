export const NAME = 'alerts'
export const DISMISS_ERROR = `${NAME}/DISMISS_ERROR`
export const DISPLAY_ERROR = `${NAME}/DISPLAY_ERROR`
