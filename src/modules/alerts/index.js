import { reducer } from './reducer'
import {NAME} from './constants'

export { dismissError, displayError } from './actions'

/**
 * ALERTS MODULE
 * Takes care of displaying error messages.
 * Only one error at-time available.
 */
export const alerts = {
    NAME,
    reducer,
}
