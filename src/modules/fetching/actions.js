import { START_FETCHING, STOP_FETCHING } from './constants'

/**
 * Creates an action which will store the name of loader into the state.
 * @param loaderName - string, name of loader (essentially a constant).
 */
export const startFetching = loaderName => ({
    type: START_FETCHING,
    loaderName,
})

/**
 * Creates an action that will remove the loader name from the state.
 * @param loaderName - string, name of loader (essentially a constant).
 */
export const stopFetching = loaderName => ({
    type: STOP_FETCHING,
    loaderName,
})
