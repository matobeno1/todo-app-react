import { reducer } from './reducer'
import { NAME } from './constants'

export { createLoaderSelector } from './selectors'
export {
    LOADER_FETCH_TODOS,
    LOADER_COMPLETE_ALL,
    LOADER_DELETE_COMPLETED,
} from './constants'
export { startFetching, stopFetching } from './actions'

/**
 * FETCHING MODULE
 * Sets the status of loaders (spinners).
 */
export const fetching = {
    NAME,
    reducer,
}
