export const NAME = "fetching";

export const START_FETCHING = `${NAME}/START_FETCHING`;
export const STOP_FETCHING = `${NAME}/STOP_FETCHING`;

// Loaders - names
export const LOADER_FETCH_TODOS = "LOADER_FETCH_TODOS";
export const LOADER_DELETE_COMPLETED = "LOADER_DELETE_COMPLETED";
export const LOADER_COMPLETE_ALL = "LOADER_COMPLETE_ALL";
