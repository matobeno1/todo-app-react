import {produce} from 'immer';

import {START_FETCHING,STOP_FETCHING} from './constants';

export const reducer = (state = [], action) => {
    if (action.type === START_FETCHING){
        return produce(state, (draft) => {
            draft.push(action.loaderName);
        })
    }
    if (action.type === STOP_FETCHING){
        return state.filter(name => name !== action.loaderName);
    }
    return state;
}
