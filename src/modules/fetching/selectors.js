/**
 * Retrieves the array where loader names (constants) are stored.
 */
const getApiArray = state => state.api

/**
 * Creates a selector based on the loaderName parameter.
 * @param loaderName - a string (esentially a constant)
 * @returns function - this function has loader name specified in it's body, returns boolean.
 */
export const createLoaderSelector = loaderName => {
    return state => getApiArray(state).includes(loaderName)
}
