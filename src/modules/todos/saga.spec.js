import { expectSaga } from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import { throwError } from 'redux-saga-test-plan/providers'

import { sagaFetchTodos, sagaCompleteAll } from './saga'
import {
    LOADER_COMPLETE_ALL,
    LOADER_FETCH_TODOS,
    startFetching,
    stopFetching,
} from '../fetching'
import * as api from '../api'
import { completeAll, storeTodos } from './actions'

describe('sagaFetchTodos', () => {
    it('should start and stop fetching todos', () => {
        return expectSaga(sagaFetchTodos)
            .put(startFetching(LOADER_FETCH_TODOS))
            .put(stopFetching(LOADER_FETCH_TODOS))
            .run()
    })

    it('should put fetched data', () => {
        const fakeData = []
        return expectSaga(sagaFetchTodos)
            .provide([[matchers.call.fn(api.fetchTodos), fakeData]])
            .put(storeTodos(fakeData))
            .run()
    })

    it('should not put fetched data if data is undefined', () => {
        const fakeData = undefined
        return expectSaga(sagaFetchTodos)
            .provide([[matchers.call.fn(api.fetchTodos), fakeData]])
            .not.put(storeTodos(fakeData))
            .run()
    })
})

describe('sagaCompleteAll', () => {
    it('should start fetching and stop it', () => {
        const ids = []
        return expectSaga(sagaCompleteAll, { ids })
            .put(startFetching(LOADER_COMPLETE_ALL))
            .put(stopFetching(LOADER_COMPLETE_ALL))
            .run()
    })

    it('should complete all todos', () => {
        const ids = [1, 2, 3, 4]
        return expectSaga(sagaCompleteAll, { ids })
            .provide([[matchers.call.fn(api.completeAll), true]])
            .put(completeAll())
            .run()
    })
})
