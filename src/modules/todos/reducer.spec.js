import { expect } from 'chai'

import { reducer } from './reducer'
import { deleteTodo, filterTodos, storeTodos, toggleTodo } from './actions'
import { FILTER_COMPLETE, FILTER_INCOMPLETE, FILTER_ALL } from './constants'

const mockTodos = [
    {
        id: '5d3e7e60-fb21-11e9-93e7-6ddbb59dd577',
        text: '2',
        completed: false,
        createdDate: 1572445632838,
    },
    {
        id: '5ce33f00-fb21-11e9-93e7-6ddbb59dd577',
        text: '11',
        completed: false,
        createdDate: 1572445632240,
    },
]

describe('todos reducer', () => {
    const mock = {
        todoFilter: 'all',
        items: [],
    }

    it('should return the initial state', () => {
        const state = reducer({}, { type: undefined })
        expect(state).to.deep.equal(mock)
    })

    describe('should handle todoFilter reducer', () => {
        it('should handle FILTER_COMPLETE', () => {
            const state = reducer({}, filterTodos(FILTER_COMPLETE))
            expect(state).to.deep.include({
                todoFilter: FILTER_COMPLETE,
            })
        })
        it('should handle FILTER_INCOMPLETE', () => {
            const state = reducer({}, filterTodos(FILTER_INCOMPLETE))
            expect(state).to.deep.include({
                todoFilter: FILTER_INCOMPLETE,
            })
        })
        it('should handle FILTER_ALL', () => {
            const state = reducer({}, filterTodos(FILTER_ALL))
            expect(state).to.deep.include({
                todoFilter: FILTER_ALL,
            })
        })
    })

    describe('should handle items reducer', () => {
        it('should handle FETCH_TODOS', () => {
            const state = reducer({}, storeTodos(mockTodos))
            expect(state).to.have.property('items')
            expect(state.items).to.deep.equal(mockTodos)
        })

        describe('should handle TOGGLE_TODOS', () => {
            let completed, todo
            before(() => {
                let { id, completed } = mockTodos[0]
                const initialState = reducer({}, {})
                const state = reducer(
                    { ...initialState, items: mockTodos },
                    toggleTodo(id, !completed)
                )

                // Find item in state
                todo = state.items.find(item => item.id === id)
            })

            it('should have property `completed` set to correct value', () => {
                expect(todo).to.have.property('completed', !completed)
            })

            it('should not mutate the original state', () => {
                expect(mockTodos[0]).to.not.deep.equal(todo)
            })
        })

        describe('should handle DELETE_TODO', () => {
            let state, todo
            before(() => {
                todo = mockTodos[0]
                state = reducer({}, storeTodos(mockTodos))
                state = reducer(state, deleteTodo(todo.id))
            })

            it('should not contain todo in items array', () => {
                expect(state.items).to.not.deep.include(todo)
            })

            it('should not mutate the previous state', () => {
                expect(mockTodos).to.not.deep.equal(state.items)
            })
        })
    })
})
