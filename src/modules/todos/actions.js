/* ------- Action creator is a function that returns an action (which is an object) ------- */

import {
    COMPLETE_ALL,
    CREATE_TODO,
    DELETE_TODO,
    EDIT_TODO,
    FETCH_TODOS,
    FILTER_TODOS,
    REQUEST_COMPLETE_ALL,
    REQUEST_CREATE_TODO,
    REQUEST_DELETE_COMPLETED,
    REQUEST_DELETE_TODO,
    REQUEST_EDIT_TODO,
    REQUEST_TOGGLE_TODO,
    TOGGLE_TODO,
    DELETE_COMPLETED
} from './constants'

/* ------- Fetch all todos ------- */

/**
 * Stores todos loaded from database.
 * @param todos - array of objects (todos)
 */
export const storeTodos = todos => ({
    type: FETCH_TODOS,
    todos,
})

/* ------- Toggle todo ------- */

export const requestToggleTodo = (id, completed) => ({
    type: REQUEST_TOGGLE_TODO,
    id,
    completed,
})

/**
 * Toggles a todo.
 * @param id - id of a todo to toggle
 * @param completed - updated value of type boolean
 */
export const toggleTodo = (id, completed) => ({
    type: TOGGLE_TODO,
    id,
    completed,
})

/* ------- Create todo ------- */

export const requestCreateTodo = text => ({
    type: REQUEST_CREATE_TODO,
    text,
})

/**
 * Appends new todo to existing list of todos.
 * @param todo - object
 */
export const createTodo = todo => ({
    type: CREATE_TODO,
    todo,
})

/* ------- Delete todo ------- */

export const requestDeleteTodo = id => ({
    type: REQUEST_DELETE_TODO,
    id,
})

export const deleteTodo = id => ({
    type: DELETE_TODO,
    id,
})

/* ------- Edit todo ------- */

export const requestEditTodo = (id, text) => ({
    type: REQUEST_EDIT_TODO,
    text,
    id,
})


/**
 * Sets the text of a specified todo.
 * @param id - id of the todo
 * @param text - new text of a todo
 */
export const editTodo = (id, text) => ({
    type: EDIT_TODO,
    text,
    id,
})

/* ------- Complete all ------- */
export const requestCompleteAll = ids => ({
    type: REQUEST_COMPLETE_ALL,
    ids,
})

/**
 * Completes all incomplete todos.
 */
export const completeAll = () => ({
    type: COMPLETE_ALL,
})

/* ------- Delete completed ------- */

export const requestDeleteCompleted = ids => ({
    type: REQUEST_DELETE_COMPLETED,
    ids,
})

/**
 * Deletes all completed todos.
 */
export const deleteCompleted = () => ({
    type: DELETE_COMPLETED
})

/* ------- Other ------- */


/**
 * Changes the type of filter that displays todos.
 * @param filterName - string, name of the filter (a constant)
 */
export const filterTodos = filterName => ({
    type: FILTER_TODOS,
    filterName,
})
