import { reducer } from './reducer'
import { saga } from './saga'
import { NAME } from './constants'

export * from './actions' // TODO ASK is this type of export okay?
export * from './selectors'
export { FILTER_INCOMPLETE, FILTER_COMPLETE, FILTER_ALL } from './constants'

/**
 * TODOS MODULE
 * Stores all todos, handles operations related to todos
 * and filtering of the todo list.
 */
export const todos = {
    NAME,
    reducer,
    saga,
}
