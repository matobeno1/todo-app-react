import { combineReducers } from 'redux'
import {produce} from 'immer';

import {
    COMPLETE_ALL,
    CREATE_TODO,
    DELETE_TODO,
    EDIT_TODO,
    FETCH_TODOS,
    FILTER_ALL,
    FILTER_TODOS,
    TOGGLE_TODO,
    DELETE_COMPLETED
} from './constants'

const todoFilter = (state = FILTER_ALL, action) => {
    if (action.type === FILTER_TODOS) {
        return action.filterName
    }
    return state
}

const items = (state = [], action) => {
    switch (action.type) {
        case FETCH_TODOS:
            return action.todos
        case TOGGLE_TODO:
            return produce(state, (draft) => {
                draft.find(todo => todo.id === action.id).completed = action.completed
            })
        case DELETE_TODO:
            return state.filter(todo => todo.id !== action.id)
        case EDIT_TODO:
            return produce(state, (draft) => {
                draft.find(todo => todo.id === action.id).text = action.text
            })
        case CREATE_TODO:
            return [...state, action.todo]
        case COMPLETE_ALL:
            return produce(state, (draft) => {
                draft.forEach(todo => todo.completed = true)
            })
        case DELETE_COMPLETED:
            return state.filter(todo => !todo.completed);
        default:
            return state
    }
}

export const reducer = combineReducers({
    todoFilter,
    items,
})
