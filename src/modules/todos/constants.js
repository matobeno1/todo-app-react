export const NAME = 'todos'

/**
 * Helper function, creates a prefixed name of an action type.
 * e.g. "todos/NAME"
 * @param typeName - name of the action type
 * @returns {string} - the prefixed name.
 */
const createActionType = typeName => `${NAME}/${typeName}`

// Filter
export const FILTER_INCOMPLETE = 'incomplete'
export const FILTER_COMPLETE = 'complete'
export const FILTER_ALL = 'all'

// Synchronous action types
export const FETCH_TODOS = createActionType('FETCH_TODOS')
export const FETCH_API = createActionType('FETCH_API')
export const CREATE_TODO = createActionType('CREATE_TODO')
export const EDIT_TODO = createActionType('EDIT_TODO')
export const DELETE_TODO = createActionType('DELETE_TODO')
export const TOGGLE_TODO = createActionType('TOGGLE_TODO')
export const FILTER_TODOS = createActionType('FILTER_TODOS')
export const COMPLETE_ALL = createActionType('COMPLETE_ALL')
export const DELETE_COMPLETED = createActionType("DELETE_COMPLETED")



// Async action types - for saga
export const REQUEST_FETCH_TODOS = createActionType('REQUEST_FETCH_TODOS')
export const REQUEST_TOGGLE_TODO = createActionType('REQUEST_TOGGLE_TODO')
export const REQUEST_DELETE_TODO = createActionType('REQUEST_DELETE_TODO')
export const REQUEST_CREATE_TODO = createActionType('REQUEST_CREATE_TODO')
export const REQUEST_EDIT_TODO = createActionType('REQUEST_EDIT_TODO')
export const REQUEST_COMPLETE_ALL = createActionType('REQUEST_COMPLETE_ALL')
export const REQUEST_DELETE_COMPLETED = createActionType(
    'REQUEST_DELETE_COMPLETED'
)
