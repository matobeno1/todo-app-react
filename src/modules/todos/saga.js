import { call, put, takeEvery, takeLatest, fork } from 'redux-saga/effects'

import {
    REQUEST_COMPLETE_ALL,
    REQUEST_CREATE_TODO,
    REQUEST_DELETE_COMPLETED,
    REQUEST_DELETE_TODO,
    REQUEST_EDIT_TODO,
    REQUEST_TOGGLE_TODO,
} from './constants'
import {
    deleteTodo,
    storeTodos,
    toggleTodo,
    createTodo,
    editTodo,
    completeAll,
    deleteCompleted,
} from './actions'
import * as api from '../api'
import {
    startFetching,
    stopFetching,
    LOADER_COMPLETE_ALL,
    LOADER_DELETE_COMPLETED,
    LOADER_FETCH_TODOS,
} from '../fetching'

export function* saga() {
    yield call(sagaFetchTodos)
    yield takeLatest(REQUEST_TOGGLE_TODO, sagaToggleTodo)
    yield takeEvery(REQUEST_DELETE_TODO, sagaDeleteTodo)
    yield takeEvery(REQUEST_EDIT_TODO, sagaEditTodo)
    yield takeEvery(REQUEST_COMPLETE_ALL, sagaCompleteAll)
    yield takeEvery(REQUEST_DELETE_COMPLETED, sagaDeleteCompleted)
    yield takeEvery(REQUEST_CREATE_TODO, sagaCreateTodo)
}

/* ------- Fetch Todos ------- */

export function* sagaFetchTodos() {
    yield put(startFetching(LOADER_FETCH_TODOS))
    const data = yield call(api.fetchTodos)
    if (data) {
        yield put(storeTodos(data))
    }
    yield put(stopFetching(LOADER_FETCH_TODOS))
}

/* ------- Toggle Todo ------- */

export function* sagaToggleTodo({ id, completed }) {
    const data = yield call(api.toggleTodo, id, completed)
    if (data) {
        yield put(toggleTodo(id, data.completed))
    }
}

/* ------- Delete Todo ------- */

export function* sagaDeleteTodo({ id }) {
    if (yield api.deleteTodo(id)) {
        yield put(deleteTodo(id))
    }
}

/* ------- Create todo ------- */

export function* sagaCreateTodo({ text }) {
    const data = yield api.createTodo(text)
    if (data) {
        yield put(createTodo(data))
    }
}

/* ------- Edit todo ------- */

export function* sagaEditTodo({ id, text }) {
    if (yield api.editTodo(id, text)) {
        yield put(editTodo(id, text))
    }
}

/* ------- Complete all ------- */

export function* sagaCompleteAll({ ids }) {
    yield put(startFetching(LOADER_COMPLETE_ALL))
    if (yield fork(api.completeAll, ids)) {
        yield put(completeAll())
    }
    yield put(stopFetching(LOADER_COMPLETE_ALL))
}

/* ------- Delete all ------- */

export function* sagaDeleteCompleted({ ids }) {
    yield put(startFetching(LOADER_DELETE_COMPLETED))
    if (yield fork(api.deleteCompleted, ids)) {
        yield put(deleteCompleted())
    }
    yield put(stopFetching(LOADER_DELETE_COMPLETED))
}
