import { createSelector } from 'reselect'

import { FILTER_COMPLETE, FILTER_INCOMPLETE, FILTER_ALL } from './index'
const FILTER_ERROR_MESSAGE =
    'Selector getVisibleTodos could not recognise provided filter name.'

const getTodos = state => state.todos.items

export const getFilter = state => state.todos.todoFilter

/**
 * Selects all the completed todos and caches them.
 */
export const getCompletedTodos = createSelector(
    getTodos,
    getTodos => getTodos.filter(todo => todo.completed)
)

/**
 * Selects all the in-complete todos and caches them.
 */
export const getIncompleteTodos = createSelector(
    getTodos,
    getTodos => getTodos.filter(todo => !todo.completed)
)

/**
 * Selects all the todos that match the todo filter criteria.
 * @throws Error - if provided filter name does not match available filter types.
 */
export const getVisibleTodos = createSelector(
    [getFilter, getTodos, getCompletedTodos, getIncompleteTodos],
    (getFilter, getTodos, getCompletedTodos, getIncompleteTodos) => {
        switch (getFilter) {
            case FILTER_COMPLETE:
                return getCompletedTodos
            case FILTER_INCOMPLETE:
                return getIncompleteTodos
            case FILTER_ALL:
                return getTodos
            default:
                throw new Error(FILTER_ERROR_MESSAGE)
        }
    }
)

/**
 * Selects amount of completed todos.
 */
export const getCompletedTodosAmount = state => {
    return getCompletedTodos(state).length
}

/**
 * Selects amount of in-complete todos.
 */
export const getIncompleteTodosAmount = state => {
    return getIncompleteTodos(state).length
}

/**
 * Selects amount of all todos.
 */
export const getTotalTodosAmount = state => {
    return getTodos(state).length
}

/**
 * Caches a list of IDs of in-complete todos.
 */
export const getIncompleteTodosIds = createSelector(
    [getIncompleteTodos],
    getIncompleteTodos => getIncompleteTodos.map(todo => todo.id)
)

/**
 * Caches a list of IDs of completed todos.
 */
export const getCompleteTodosIds = createSelector(
    [getCompletedTodos],
    getCompletedTodos => getCompletedTodos.map(todo => todo.id)
)
